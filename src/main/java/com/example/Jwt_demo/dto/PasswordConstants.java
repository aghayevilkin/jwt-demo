package com.example.Jwt_demo.dto;

public class PasswordConstants {
    public static final int PASSWORD_MIN_LENGTH = 3;
    public static final int PASSWORD_MAX_LENGTH = 16;
}
