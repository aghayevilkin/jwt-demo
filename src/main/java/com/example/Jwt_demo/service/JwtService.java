package com.example.Jwt_demo.service;

import com.example.Jwt_demo.config.SecurityProperties;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import java.security.Key;
import java.time.Duration;
import java.time.Instant;
import java.util.Date;
import java.util.Set;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public final class JwtService {

//    private final Set<ClaimSetProvider> claimSetProviders;
    private final SecurityProperties applicationProperties;

    private Key key;

    @PostConstruct
    public void init() {
        byte[] keyBytes;
        keyBytes = Decoders.BASE64.decode(applicationProperties.getJwtProperties().getSecret());
        key = Keys.hmacShaKeyFor(keyBytes);
    }

    public Claims parseToken(String token) {
        return Jwts.parserBuilder()
                .setSigningKey(key)
                .build()
                .parseClaimsJws(token)
                .getBody();
    }

    public String issueToken(Authentication authentication, Duration duration) {
        log.trace("Issue JWT token to {} for {}", authentication.getPrincipal(), duration);

        Set<String> authorities = authentication.getAuthorities().stream()
                .map(Object::toString)
                .collect(Collectors.toSet());

        final JwtBuilder jwtBuilder = Jwts.builder()
                .setSubject(authentication.getName())
                .setIssuedAt(new Date())
                .setExpiration(Date.from(Instant.now().plus(duration)))
                .signWith(key, SignatureAlgorithm.HS512)
                .claim("rule",authorities);

        return jwtBuilder.compact();
    }

//    private JwtBuilder addClaimsSets(JwtBuilder jwtBuilder, Authentication authentication) {
//        claimSetProviders.forEach(claimSetProvider -> {
//            final ClaimSet claimSet = claimSetProvider.provide(authentication);
//            log.trace("Adding claim {}", claimSet);
//            jwtBuilder.claim(claimSet.getKey(), claimSet.getClaims());
//        });
//        return jwtBuilder;
//    }

//    private JwtBuilder addClaims(JwtBuilder jwtBuilder, Authentication authentication) {
//        claimProviders.forEach(claimSetProvider -> {
//            final Claim claim = claimSetProvider.provide(authentication);
//            log.trace("Adding claim {}", claim);
//            jwtBuilder.claim(claim.getKey(), claim.getClaim());
//        });
//        return jwtBuilder;
//    }

}
