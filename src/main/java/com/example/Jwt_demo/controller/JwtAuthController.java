package com.example.Jwt_demo.controller;

import com.example.Jwt_demo.dto.LoginDto;
import com.example.Jwt_demo.service.JwtService;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.time.Duration;

@Slf4j
@RestController
@RequiredArgsConstructor
public class JwtAuthController {

    private static final Duration ONE_DAY = Duration.ofDays(1);
    private static final Duration ONE_HOUR = Duration.ofHours(1);


    private final JwtService jwtService;
    private final AuthenticationManagerBuilder authenticationManagerBuilder;

    @GetMapping("/admin")
    public String test(){
        return "Hello World";
    }
    @PostMapping("/authenticate")
    public ResponseEntity<JwtToken> authorize(@Valid @RequestBody LoginDto loginDto) {

        /** Authenticated USER and Got user details from DB **/
        log.trace("Authenticating user {}", loginDto.getUsername());
        Authentication authenticationToken = new UsernamePasswordAuthenticationToken(loginDto.getUsername(), loginDto.getPassword());

        log.info("My authenticationToken token {}", authenticationToken);
        Authentication authentication = authenticationManagerBuilder.getObject().authenticate(authenticationToken);
        log.info("My authenticationToken token {}", authentication);
        log.info("Created authentication {}", authentication);

        /** Generate JSON Web Token based on user details from step 1 **/
        Duration duration = getDuration(loginDto.getRememberMe());
        String jwt = jwtService.issueToken(authentication,duration);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(HttpHeaders.AUTHORIZATION, "Bearer " + jwt);
        return new ResponseEntity<>(new JwtToken(jwt), httpHeaders, HttpStatus.OK);
    }

    /**
     * Object to return JWT as Authentication body.
     */
    private class JwtToken {

        private String idToken;

        JwtToken(String idToken) {
            this.idToken = idToken;
        }

        @JsonProperty("id_token")
        String getIdToken() {
            return idToken;
        }

        void setIdToken(String idToken) {
            this.idToken = idToken;
        }
    }

    private Duration getDuration(Boolean rememberMe) {
        return ((rememberMe != null) && rememberMe) ? ONE_DAY : ONE_HOUR;
    }
}
