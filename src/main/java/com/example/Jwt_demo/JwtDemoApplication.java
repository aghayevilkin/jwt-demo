package com.example.Jwt_demo;

import com.example.Jwt_demo.repository.UserRepository;
import com.example.Jwt_demo.domain.User;
import com.example.Jwt_demo.service.JwtService;
import io.jsonwebtoken.Claims;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

@SpringBootApplication
@RequiredArgsConstructor
@Component
@EnableGlobalMethodSecurity(
		prePostEnabled = true,
		securedEnabled = true,
		jsr250Enabled = true)
public class JwtDemoApplication implements CommandLineRunner {

	private final JwtService jwtService;
	private final UserRepository userRepository;

	private final BCryptPasswordEncoder passwordEncoder;


	public static void main(String[] args) {
		SpringApplication.run(JwtDemoApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		User user = new User();
		user.setPassword(passwordEncoder.encode("1234"));
		user.setUsername("admin");
		userRepository.save(user);
		////		jwtService.parseToken();
		Claims claims = jwtService.parseToken("eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImlhdCI6MTY1NjIyMTY1MCwiZXhwIjoxNjU2MzA4MDUwfQ.6jne-_QfZx6Bhnf_H3gxBWi0w0KkmWrLYpPIsbDLhy4pA2v302qvZvJD6reivTe9szMx6nNp4DBpZ-5wm5bKFA");
		System.out.println(claims);
	}
}
